import time
from typing import Callable

import craft_text_detector.predict as predict
import torch
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor
from vietocr.tool.config import Cfg

# Config Detectron2
torch.backends.cudnn.benchmark = True
config_path = "config.yml"
cfg = get_cfg()
cfg.merge_from_file(config_path)
cfg.MODEL.DEVICE = 'cpu'
predictor = DefaultPredictor(cfg)

# Config CRAFT
get_prediction = predict.get_prediction

# Config Viet OCR
config = Cfg.load_config_from_name('vgg_transformer')  # sử dụng config mặc định của mình
config['device'] = 'cpu'  # device chạy 'cuda:0', 'cuda:1', 'cpu'


def measure_duration_processing_async(func: Callable):
    async def inner(*args, **kwargs):
        start = time.time()
        result = await func(*args, **kwargs)
        end = time.time()
        print("------------ {} spends {} seconds".format(func.__name__, end - start))
        return result

    return inner


def measure_duration_processing_sync(func: Callable):
    def inner(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print("------------ {} spends {} seconds".format(func.__name__, end - start))
        return result

    return inner
