# Ubuntu 20.04 have Py pre-installed
FROM ubuntu:20.04

RUN apt update
RUN apt -y upgrade
RUN apt install -y python3-pip
RUN apt install -y build-essential libssl-dev libffi-dev python3-dev

# Install Git for cloning some repositories as sub module
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git

# Set timezone, avoid Tess asking
ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Copy src code into Container
WORKDIR /graduation_thesis_fastapi
COPY . /graduation_thesis_fastapi/

# Create venv, activate
RUN apt-get install python3-venv --yes
RUN python3 -m venv ocr_env
RUN /bin/bash -c "source ocr_env/bin/activate"

# Install requirements
# Upgrade pip, install basic requirements
RUN python3 -m pip install --upgrade pip
RUN pip install -r requirements.txt
# For Spacy: Vietnamese language model
RUN pip install https://github.com/trungtv/vi_spacy/raw/master/packages/vi_spacy_model-0.2.1/dist/vi_spacy_model-0.2.1.tar.gz
# For Detectron2
RUN pip install git+https://github.com/facebookresearch/fvcore.git
RUN pip install -e detectron2_repo

# Tesseract:
# Install real Tesseract for Python wrapper
RUN apt-get update
RUN apt-get install tesseract-ocr-vie --yes
# # Move the newest pretrained models into the right location for Tesseract's calling
# COPY ./traineddata/vie.traineddata /usr/share/tesseract-ocr/4.00/tessdata/vie.traineddata
# COPY ./traineddata/eng.traineddata /usr/share/tesseract-ocr/4.00/tessdata/eng.traineddata

# Just update
RUN apt-get update
RUN apt-get install ffmpeg libsm6 libxext6  -y

# Run in img
EXPOSE 13000
CMD ["uvicorn", "main:app", "--reload", "--host", "0.0.0.0", "--port", "13000"]
